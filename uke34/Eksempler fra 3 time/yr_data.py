# For denne må du importere pakken 'python-yr'
# https://www.yr.no/sted/Norge/Trøndelag/Trondheim/Trondheim/time_for_time.html

from yr.libyr import Yr

import json
import matplotlib.pyplot as plt

weather = Yr(location_name="Tyskland/Berlin/Berlin", forecast_link='forecast_hour_by_hour')        
#now = weather.now(as_json=True)

temperature = []

for forecast in weather.forecast(str):
    data = json.loads(forecast)
    temperature.append(int(data['temperature']['@value']))
    
print(temperature)

plt.plot(temperature)
plt.show()
