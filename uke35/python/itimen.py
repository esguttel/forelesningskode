'''
beregne inntekt
få inn timelønn
få inn timer
gange timelønn med timer
skrive ut svaret
'''

def beregne(pertime, antalltimer):
    return pertime * antalltimer

pertime = float(input("Timelønn: "))
antalltimer = float(input("Antall timer: "))
print(f"Du har tjent {format(beregne(pertime, antalltimer), '5.2f')}.")
print(f"Du har tjent {beregne(pertime, antalltimer):5.2f}.")

